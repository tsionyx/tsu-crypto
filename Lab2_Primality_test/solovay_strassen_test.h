#pragma once
#ifndef SOLOVAY_STRASSEN

#define SOLOVAY_STRASSEN
#include "../Lab1_Large_numbers/__uintX2.h"
#include <math.h>

// Solovay�Strassen primality test
static bool probably_prime(const uint128 n,const double err=0.1)
{
	if((err>=1)||(err<=0)) throw "Error possibility should be between 0 and 1";
	if((n&1)==0) return false;
	const unsigned __int8 T=(int)(ceil((double)log(1/err)/log(2.0)));
	for(unsigned __int8 i=0;i<T;++i)
	{
		uint128 a=uint128::random()%(n-3)+2;
		if(uint128::gcd(a,n)!=1) return false;
		signed __int8 r=uint128::jacobi_symbol(a,n);
		if(r==0) return false;
		uint128 s=uint128::pow_m(a,(n-1)/2,n);

		if(((r==1)&&(s!=1))
		  ||((r==-1)&&(s!=n-1))) 
		  return false; //Euler's criterion
	}
	return true;
}
#endif /* SOLOVAY_STRASSEN */