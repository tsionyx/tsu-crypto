#pragma once
class elliptic_curve;

struct ec_point
{
	ec_point(const elliptic_curve * const ec, bool is_inf = false);
	ec_point(const elliptic_curve * const ec, const double _x, const double _y);
	const bool inf;
	double x,y;
	const elliptic_curve *const container;
	ec_point operator +(const ec_point p);
	friend ec_point operator -(const ec_point p);
	bool operator ==(const ec_point p);
	inline bool same_curve(const ec_point p);
};

class elliptic_curve
{
public:
	elliptic_curve(const double a, const double b);
	bool contains(const ec_point p) const;
	double a() const;
	double b() const;
private:
	//parameters
	const double _a,_b;
};
