#include "stdafx.h"

#define TEST
#define SCREEN_LOG
#include "BS_GS.h"
#include "../Lab4_Diffie_Hellman/DH_System.h"
using namespace std;

large_uint break_Diffie_Hellman(const large_uint& a_msg,
								const large_uint& b_mesage,
								const large_uint& gen,
								const large_uint& mod)
{
	//break Alice's private key
	large_uint a_RK = get_discrete_log(a_msg,gen,mod);
	return large_uint::pow_m(b_mesage,a_RK,mod);
	
	/*
	//or Bob's private key
	large_uint b_RK = get_discrete_log(b_msg,gen,mod);
	return large_uint::pow_m(a_mesage,b_RK,mod);
	*/
}

int main()
{
#ifdef TEST

	const unsigned int N = 5;
	large_uint p[N]={373063189,20945261,4323257,24785021,6892159};
	large_uint g[N]={3,7,5,2,5};
	large_uint A_key[N]={35,1934,56,99,183};
	large_uint B_key[N]={21,751,22,161,143};
	large_uint SH_key[N]={160588074,12404349,4176228,11587559,6734476};

	for(unsigned int i=2;i<3;++i)
	{
		DH_System d(p[i],g[i],A_key[i],B_key[i]);
		d.key_exchange();

		//clock_t begin = clock();
		large_uint breakedSH = break_Diffie_Hellman(d.get_a_msg(),
			                                        d.get_b_msg(),
													d.get_g(),
													d.get_p());
		//clock_t end = clock();
		//cout<<"CPU cycles: "<<end-begin<<endl;

		assert(breakedSH==SH_key[i]);
		
#ifdef SCREEN_LOG
		cout<<"Shared key is found successfully: "<<breakedSH<<"\n";
#endif /* SCREEN LOG */
	}
	return 0;
#endif /* TEST */
	uint64 mod,gen,res;
	cout<<"\'q\' to exit\n";

	while(true)
	{
		
		//cout<<"mod:";cin>>mod;
		mod = 772;
		//cout<<"base:";cin>>gen;
		gen = 2;

		cout<<"res:";cin>>res;
		if((char)res == 'q') break;

		cout<<gen<<" ^ "
			<<get_discrete_log(res, gen, mod)<<" = "
			<<res<<" (mod "<<mod<<")"<<endl;
	}
	return 0;
}

