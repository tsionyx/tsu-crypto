#pragma once
#include <string>
#include "secret_sharing.h"


template<typename shareT, typename secretT>
class ts_dealer: public ss_dealer<shareT, secretT>
{
public:
    ts_dealer(const size_t threshold, const size_t number)
    {
        if (number < 2) throw "The number of players in threshold scheme must be greater than 2";
        if(threshold > number) throw "The threshold value in threshold scheme must be less than total number of players";

         _threshold = threshold, _number = number;
    }

    virtual void distribute_secret(
        const ss_secret<secretT> *const secret,
        const ss_players<shareT>& players) const
    {
        if(_number != players.size())
        {
            std::string error_message = "Number of players should be equal to ";
            //std::ostringstream oss;
            //oss << error_message << _number;
            //std::string output(oss.str());
            //throw output;
            throw error_message;
        }
    }

    virtual ss_secret<secretT> *const restore_secret(
         const ss_players<shareT>& players) const
     {
         if(players.size() < this->_threshold) throw "Not enough players to restore the secret";
         return NULL;
     }
protected:
    size_t _threshold, _number;
   
};

/*
template<typename shareT, typename secretT>
class threshold_scheme: public secret_sharing<shareT, secretT>
{
public:
    threshold_scheme(const size_t threshold, const size_t number, ss_dealer<shareT, secretT> *const dealer)
        :secret_sharing(dealer)
    {
        if (number < 2) throw "The number of players in threshold scheme must be greater than 2";
        if(threshold > number) throw "The threshold value in threshold scheme must be less than total number of players";

         _threshold = threshold, _number = number;
    }

    virtual void distribute_secret(
        const ss_secret<secretT>& secret,
        const ss_players<shareT> players) const
    {
        size_t n = players.size();
        if(n == _number)
			secret_sharing::distribute_secret(secret, players, _threshold);
        else 
        {
            std::string error_message = "Number of players should be equal to ";
            //std::ostringstream oss;
            //oss << error_message << _number;
            //std::string output(oss.str());
            //throw output;
            throw error_message;
        }
    }
protected:
    size_t _threshold, _number;

    bool is_able_to_restore(const size_t players) const
    {
        if(players < this->_threshold) return false;
        return true;
    }
};
*/