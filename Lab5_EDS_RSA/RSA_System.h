#pragma once

#ifndef RSA_SYSTEM

#define RSA_SYSTEM

#include "../Lab1_Large_numbers/__uintX2.h"
#include "../Lab2_Primality_test/solovay_strassen_test.h"
//#include "../Lab3_Primality_test/poklington_test.h"

typedef uint128 large_uint;

#ifndef RSA_MOD_SIZE
#define RSA_MOD_SIZE sizeof(large_uint)*8
#endif

class RSA_System
{
public:
	RSA_System();
//#ifdef TEST
	RSA_System(large_uint p,large_uint q, large_uint d);
//#endif 

	large_uint get_RSA_mod() const;
	large_uint get_RSA_exp() const;
	large_uint get_signature(const large_uint &x) const;
protected:
	void create_RSA_mod();
	void create_e();

	inline large_uint get_n() const;
	inline large_uint get_totient_n() const;
	large_uint p,q,d,e;

	//large_uint n,e;
};


class RSA_Verifyer
{
public:
	RSA_Verifyer(large_uint n, large_uint e);
	bool verify(const large_uint &x, const large_uint &s);
private:
	large_uint _n,_e;
};
#endif /* RSA_SYSTEM */