#include "stdafx.h"

//#define SCREEN_LOG
#define TEST
#include "RSA_System.h"

int main()
//int main(int argc, char* argv[])
{
#ifdef TEST
	const int N = 5;
	large_uint x[N] = {103, 8008, 802, 22, 153};
	large_uint p[N] = {39503, 6197, 24907, 4721, 65809};
	large_uint q[N] = {71983, 3491, 9049, 37963, 3767};

	large_uint n[N] = {2843544449, 21633727, 225383443, 179223323, 247902503};
	large_uint t[N] = {2843432964, 21624040, 225349488, 179180640, 247832928};

	large_uint e[N] = {37, 93, 47, 83, 65};
	large_uint d[N] = {537946777, 13718477, 115072079, 79875707, 68630657};

	large_uint s[N] = {934219078, 21294134, 66278188, 164775041, 43942564};

	for(int i = 0; i < N; ++i)
	{
		large_uint nn = p[i]*q[i];
		assert(nn==n[i]); // n = p * q
		large_uint tt = (p[i]-1)*(q[i]-1);
		assert(tt==t[i]); //phi(n) = (p - 1) * (q - 1)

		assert(e[i].inv(tt)==d[i]); // d * e == 1 (mod n)

		RSA_System sig(p[i],q[i],d[i]);
		large_uint ss = sig.get_signature(x[i]);
		assert(ss==s[i]);
		RSA_Verifyer ver(sig.get_RSA_mod(), sig.get_RSA_exp());
		assert(ver.verify(x[i],ss));
		std::cout<<'.';
	}
	
	return 0;
#endif
	uint128 a(12345,45678907778), b, f(8978990343,1549003545445);
	//uint128 a(8), b, f(15);

	RSA_System r_s;
	large_uint sign = r_s.get_signature(a);

	RSA_Verifyer verif(r_s.get_RSA_mod(), r_s.get_RSA_exp());
	bool ver = verif.verify(a,sign);
	
	std::cout<<ver<<"\n";
	return 0;
}
