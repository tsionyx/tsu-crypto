#include "stdafx.h"

#define SCREEN_LOG
//#define TEST

#include "DH_System.h"

//int main(int argc, char* argv[])

int main()
{
#ifdef TEST
	const unsigned int N = 5;
	uint128 p[N]={373063189,20945261,4323257,24785021,6892159};
	uint128 g[N]={3,7,5,2,5};
	uint128 A_key[N]={35,1934,56,99,183};
	uint128 B_key[N]={21,751,22,161,143};
	uint128 SH_key[N]={160588074,12404349,4176228,11587559,6734476};

	for(unsigned int i=0;i<N;++i)
	{
		DH_System d(p[i],g[i],A_key[i],B_key[i]);
		d.key_exchange();
		assert(d.A->SK==SH_key[i]);
		std::cout<<".";
	}
	return 0;
#endif /* TEST */
	DH_System d;
	std::cout<<"Generation complete\n";
	std::cout<<"p: "<<d.get_p()<<"\n";
	std::cout<<"g: "<<d.get_g()<<"\n";

	d.key_exchange();
	//std::cout<<"Alice's shared key: "<<d.A->SK<<"\n";
	//std::cout<<"Bob's shared key: "<<d.B->SK<<"\n";
	return 0;
}