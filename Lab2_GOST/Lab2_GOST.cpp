#include "stdafx.h"
#include "GOST_Cipher.h"
#include "FileCipher.h"

int main()
{
	//block iv(1,1);
	uint key[]={0,1,2,3,4,5,6,7,10,11,12,13,14,15,16,17,20,21,22,23,24,25,26,27};
	GOST_Cipher c(key);

	FileCipher::Encipher("Tux.bmp","encoded",&c,FE);
	FileCipher::Decipher("encoded","decode.bmp",&c,FE);
	return 0;
}


//1.add E and D to .h and .cpp
//2.change KEY_LENGTH and add SINGLE
//3.change KEY_LENGTH with SINGLE in feistel