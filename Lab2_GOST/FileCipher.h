#pragma once
#include "Block_Cipher.h"

class FileCipher
{
private:
	static void read_file(const char *const in_name, text *const t);
	static void write_file(const char *const out_name,const text *const res);
public:
	static void Encipher(
			const char *const plain_file,
			const char *const enc_file, 
			Block_Cipher *c, 
			MODE mode=ECB);
	static void Decipher(
			const char *const enc_file,
			const char *const dec_file, 
			Block_Cipher *c, 
			MODE mode=ECB);
	static init_vector Get_Hash(
			const char *const plain_file,
			Block_Cipher *c);
};