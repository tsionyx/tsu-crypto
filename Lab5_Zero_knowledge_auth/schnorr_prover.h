#pragma once
#ifndef SCHNORR_SIGNER

#define SCHNORR_SIGNER

#include "../Lab1_Large_numbers/__uintX2.h"
#include "../Lab2_Primality_test/solovay_strassen_test.h"
//#include "../Lab3_Primality_test/poklington_test.h"
//#include "schnorr_system.h"
class schnorr_system;

typedef uint128 large_uint;


class schnorr_prover
{
public:
	schnorr_prover(const schnorr_system* const parent);
	schnorr_prover(const large_uint& private_key, const schnorr_system* const parent);

	large_uint get_witness(const large_uint& random = 0);
	large_uint get_response(const large_uint& challenge) const;


	inline large_uint get_public() const { return v; }
private:
	void generate_parameters(const large_uint& private_key = 0);
	
	large_uint s,v,r;

	const schnorr_system* parent_system;
};



#endif /* SCHNORR_SIGNER */
