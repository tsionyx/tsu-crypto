#pragma once

#ifndef SCHNORR_SYSTEM

#define SCHNORR_SYSTEM

#include "..\Lab1_Large_numbers\__uintX2.h"
#include "schnorr_prover.h"



class schnorr_system
{
public:
	schnorr_system();
	schnorr_system(large_uint _p, large_uint _q, large_uint _g, size_t _t);

	// verify specified prover's message
	bool verify(const large_uint& witness, const large_uint& response, const large_uint& random_e);
	
	inline large_uint get_p() const { return p; }
	inline large_uint get_q() const { return q; }
	inline large_uint get_alpha() const { return alpha; }
	inline size_t get_security_param() const { return t; }

	inline void set_prover(const schnorr_prover* const prover)
	{
		_prover = prover;
	}

protected:
	// generates shared parameters
	void generate_parameters();
	void generate_alpha(unsigned int g = 2);
	large_uint p,q,alpha;
	size_t t;
	
	const schnorr_prover* _prover;
};

#endif /* SCHNORR_SYSTEM */