#pragma once

#ifndef DSA_SYSTEM
/* http://en.wikipedia.org/wiki/Digital_Signature_Algorithm */
#define DSA_SYSTEM

#include "..\Lab1_Large_numbers\__uintX2.h"
#include "DSA_signer.h"

#include <vector>
using std::vector;
typedef vector<DSA_signer* const> DSA_signers;

class DSA_System
{
public:
	DSA_System();
	~DSA_System();

	const DSA_signer& add_signer();
	const DSA_signer& add_signer(const large_uint& private_key);
	void remove_signer(const size_t signer_index);
	
	// verify specified signer's message
	bool verify(const size_t signer_index, const large_uint &message_hash, const DSA_signature);
	
	large_uint get_subliminal_message(const large_uint signer_private_key, const large_uint &message_hash, const DSA_signature);

	inline large_uint get_p() const { return p; }
	inline large_uint get_q() const { return q; }
	inline large_uint get_g() const { return g; }



protected:
	// generates shared parameters
	void generate_parameters();
	large_uint p,q,g;

	DSA_signers _signers;
	
};

#endif /* DSA_SYSTEM */