// Lab5_EDS_DSA.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "DSA_System.h"
#include "..\Lab2_Primality_test\solovay_strassen_test.h"
int main(int argc, char* argv[])
{
	//probably_prime(2870925763, 0.0001);
	DSA_System d;
	DSA_signer signer = d.add_signer();
	
	large_uint message = 12445565;
	DSA_signature sign = signer.get_signature(message);

	bool res = d.verify(0, message, sign);
	printf("The signature is ");
	printf(res ? "true\n": "false\n");
	d.remove_signer(0);

	large_uint hidden_message = 1234;
	large_uint private_key = 5888;
	DSA_signer signer2 = d.add_signer(private_key);

	sign = signer2.get_signature_with_subliminal_channel(message, hidden_message);
	res = d.verify(0, message, sign);
	printf("The signature2 is ");
	printf(res ? "true\n": "false\n");
	large_uint restored_hidden = d.get_subliminal_message(private_key, message, sign);

	printf("The hidden message is ");
	printf(restored_hidden == hidden_message ? "": "not ");
	printf("restored\n");
	d.remove_signer(0);
}

