#pragma once
#ifndef DSA_SIGNER

#define DSA_SIGNER

#include "../Lab1_Large_numbers/__uintX2.h"
#include "../Lab2_Primality_test/solovay_strassen_test.h"
//#include "../Lab3_Primality_test/poklington_test.h"
//#include "DSA_System.h"
class DSA_System;

typedef uint128 large_uint;
struct DSA_signature
{
public:
	large_uint r,s;
};

class DSA_signer
{
public:
	DSA_signer(const DSA_System* const parent = NULL);
	DSA_signer(const large_uint& private_key, const DSA_System* const parent = NULL);

	DSA_signature get_signature(const large_uint &x) const;


	DSA_signature get_signature_with_subliminal_channel(const large_uint& m, const large_uint& subliminal) const;


	inline large_uint get_public() const { return y; }
private:
	void generate_parameters();
	void generate_parameters(const large_uint& private_key);
	
	large_uint x,y;

	const DSA_System* parent_system;
};



#endif /* DSA_SIGNER */
