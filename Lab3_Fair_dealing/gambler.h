#pragma once
#include <vector>
#include <string>

#include "card_deck.h"

enum received_action
{
	classify,
	declassify,
	pass_on
};
class gambler;
typedef void (*card_received_callback)(const gambler& other, classifiable_card& card, const received_action action);
typedef void (*deck_received_callback)(const gambler& other, card_set& deck, const received_action action);

class gambler
{
public:
	gambler();
	gambler(const std::string name, const classifying_key _classifying_key, const classifying_key _declassifying_key);
	void take_hand(card_deck& deck, const size_t hand_size);
	void add_to_hand(classifiable_card& card);
	
	void classify_card(classifiable_card& card) const;
	void classify_deck(card_set* const deck) const;
	void unclassify_card(classifiable_card& card) const;
	void unclassify_deck(card_set* const deck) const;
	
	void pass_card(gambler& adressee, classifiable_card& card, received_action action, const gambler& adresser);
	void pass_deck(gambler& adressee, card_set* const, received_action action, const gambler& adresser);
	
	void pass_card(gambler& adressee, classifiable_card& card, received_action action);
	void pass_deck(gambler& adressee, card_set* const deck, received_action action);
	
	//inline void set_card_received_callback(const card_received_callback){	on_receieved_card = callback;}
	//inline void set_deck_received_callback(const deck_received_callback){	on_receieved_deck = callback;}
	void take_card(const gambler& other, classifiable_card& card, const received_action action);
	void take_deck(const gambler& other, card_set* const deck, const received_action action);


	// selectors
	inline const char* get_name() const;
	inline classifiable_card& get_received_card() const
	{
		return *received_card;
	}
	inline card_set& get_received_set() const
	{
		return *received_deck;
	}
	inline const gambler& get_sender() const
	{
		return *sender;
	}
	inline card_set& get_hand()
	{
		return _hand;
	}

private:
	std::string _name;

	//card_received_callback on_receieved_card;
	//deck_received_callback on_receieved_deck;

	void card_received(const gambler& other, classifiable_card& card, const received_action action);
	void deck_received(const gambler& other, card_set* const deck, const received_action action);

	card_set _hand;
	void unclassify_hand();


	classifying_key _classifying, _declassifying;

	classifiable_card* received_card;
	card_set* received_deck;

	const gambler* sender;
};

