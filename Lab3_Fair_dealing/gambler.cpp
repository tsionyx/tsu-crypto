#include "StdAfx.h"
#include "gambler.h"

gambler::gambler()
{
	throw "Not allowed to create default instance of the class";
}
gambler::gambler(const std::string name, const classifying_key _classifying_key, const classifying_key _declassifying_key)
	:_name(name),
	_classifying(_classifying_key),
	_declassifying(_declassifying_key)
{
	printf("%s is created\n",get_name());
	//on_receieved_card = NULL;
	//on_receieved_deck = NULL;
}

void gambler::take_hand(card_deck& deck, size_t hand_size)
{
	for(size_t i = 0; i < hand_size; ++i)
	{
		_hand.push_back(deck.get_random_card());
	}
	printf("%s is getting the hand\n",get_name());
}
void gambler::add_to_hand(classifiable_card& card)
{
	_hand.push_back(card);
	printf("%s is getting the card from collected\n",get_name());
}

/*
void gambler::unclassify_hand()
{
	std::for_each(_hand.begin(), _hand.end(), [&](card_status st)
	{
		unclassify_card(st.card);
	});
	printf("%s is getting the hand\n",get_name());
	_hand.print_content();
}
*/

void gambler::classify_card(classifiable_card& card) const
{
	card.classify(_classifying);
	printf("%s is classify the card\n",get_name());
}
void gambler::classify_deck(card_set* const deck) const
{
	deck->classify(_classifying);
	printf("%s is classify the deck\n",get_name());
}
void gambler::unclassify_card(classifiable_card& card) const
{
	card.declassify(_declassifying);
	printf("%s is declassify the card\n",get_name());
}
void gambler::unclassify_deck(card_set* const deck) const
{
	deck->declassify(_declassifying);
	printf("%s is declassify the deck\n",get_name());
}

void gambler::pass_card(gambler& adressee, classifiable_card& card, received_action action, const gambler& adresser)
{
	printf("%s is pass the card to %s\n",get_name(), adressee.get_name());
	adressee.take_card(adresser,card,action);
}
void gambler::pass_deck(gambler& adressee, card_set* const deck, received_action action, const gambler& adresser)
{
	printf("%s is pass the deck to %s\n",get_name(), adressee.get_name());
	adressee.take_deck(adresser, deck,action);
}

void gambler::pass_card(gambler& adressee, classifiable_card& card, received_action action)
{
	pass_card(adressee, card, action, *sender);
}
void gambler::pass_deck(gambler& adressee, card_set* const deck, received_action action)
{
	pass_deck(adressee, deck, action, *sender);
}

void gambler::take_card(const gambler& other, classifiable_card& card, const received_action action)
{
	printf("%s is receive the card from %s\n",get_name(), other.get_name());
	card_received(other,card,action);
}
void gambler::take_deck(const gambler& other, card_set* const deck, const received_action action)
{
	printf("%s is receive the deck from %s\n",get_name(), other.get_name());
	deck_received(other,deck,action);
}

void gambler::card_received(const gambler& other, classifiable_card& card, const received_action action)
{
	switch(action)
	{
	case classify:
		classify_card(card);
		break;
	case declassify:
		unclassify_card(card);
		break;
	case pass_on:
		break;
	}
	received_card = &card;
	sender = &other;
}
void gambler::deck_received(const gambler& other, card_set* const deck, const received_action action)
{
	switch(action)
	{
	case classify:
		classify_deck(deck);
		break;
	case declassify:
		unclassify_deck(deck);
		break;
	case pass_on:
		break;
	}
	received_deck = deck;
	sender = &other;
}

const char* gambler::get_name() const
{
	return _name.c_str();
}

//classifiable_card& gambler::get_received_card() const
//{
//	return *received_card;
//}

//card_set&  gambler::get_received_deck() const
//{
//	return *received_deck;
//}
//card_set& gambler::get_hand()
//{
//	return _hand;
//}
//const gambler& gambler::get_sender() const
//{
//	return *sender;
//}


