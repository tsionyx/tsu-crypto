#pragma once


#include "card_enums.h"
class card
{
protected:
	char symbol[3];
	union
	{
		unsigned __int32 single_value;
		struct 
		{
			card_rank _r;
			card_suit _s;
			unsigned __int16 therest;
		} v_un_s;
	} v_un;

#define _rank v_un.v_un_s._r
#define _suit v_un.v_un_s._s
#define _value v_un.single_value
	void build_symbol();


public:
	card();
	card(card_rank rank, card_suit suit);
	
	inline const char* const get_symbol() const
	{
		return symbol;
	}
	inline bool operator==(const card& other)
	{
		return _value==other._value;
	}
	inline bool operator!=(const card& other)
	{
		return !operator==(other);
	}
};
