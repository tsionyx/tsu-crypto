// Lab3_Fair_dealing.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "gambler.h"

#include "..\Lab1_Large_numbers\__uintX2.h"

#define print_line printf("==================================================================\n")
typedef __uintX2<unsigned __int16> uint32;
int main(int argc, char* argv[])
{
	/*
	printf("%i\n",uint128((unsigned int)(19)).inv(72072)._l);
	printf("%i\n",uint128((unsigned int)(5)).inv(72072)._l);
	printf("%i\n",uint128((unsigned int)(17)).inv(72072)._l);
	*/



	key_type large_prime = 72073;
	classifying_key c0 = {19,large_prime}, d0 = {56899,large_prime};
	classifying_key c1 = {5,large_prime}, d1 = {28829,large_prime};
	classifying_key c2 = {17,large_prime}, d2 = {63593,large_prime};
	gambler g0("Gambler0", c0, d0);
	gambler g1("Gambler1", c1, d1);
	gambler g2("Gambler2", c2, d2);
	print_line;

	/** /
	card_deck cd;
	std::for_each(cd.begin(),cd.end(),[&g2](classifiable_card& ccd)
	{
		classifiable_card old_card = ccd;
		
		ccd.print_value();
		g2.classify_card(ccd); 
		ccd.print_value();
	
		g2.unclassify_card(ccd);
		ccd.print_value();
		print_line;
		assert(ccd == old_card);// printf("ACHTUNG!!!\n");
	});
	
	/**/
	
	// create
	card_deck cd;
	//cd.shuffle();
	printf("\n\nPlain text deck\n");
	cd.print_content();print_line;

	// circle classify 
	g0.classify_deck(&cd);
	g0.pass_deck(g1, &cd, classify, g0);
	g1.pass_deck(g2, &g1.get_received_set(),classify);
	print_line;

	printf("\n\nCipher text deck\n");
	cd.print_content();print_line;

	// get the g0's hand
	g0.take_hand(cd,6);
	g0.pass_deck(g1, &g0.get_hand(), declassify, g0);
	g1.pass_deck(g2, &g1.get_received_set(),declassify);
	g0.unclassify_deck( &g0.get_hand());
	printf("\n\nG0's hand\n");
	g0.get_hand().print_content();print_line;

	// get the g1's hand
	g1.take_hand(cd,6);
	g1.pass_deck(g2, &g1.get_hand(), declassify, g1);
	g2.pass_deck(g0, &g2.get_received_set(),declassify);
	g1.unclassify_deck( &g1.get_hand());
	printf("\n\nG1's hand\n");
	g1.get_hand().print_content();print_line;

	// get the g2's hand
	g2.take_hand(cd,6);
	g2.pass_deck(g0, &g2.get_hand(), declassify, g2);
	g0.pass_deck(g1, &g0.get_received_set(),declassify);
	g2.unclassify_deck( &g2.get_hand());
	printf("\n\nG2's hand\n");
	g2.get_hand().print_content();print_line;

	// get the card from collected
	classifiable_card card = cd.get_random_card();
	g0.pass_card(g1, card, declassify, g0);
	g1.pass_card(g2, g1.get_received_card(),declassify);
	g0.unclassify_card(card);
	g0.add_to_hand(card);
	printf("\n\nG0's newed hand\n");
	g0.get_hand().print_content();print_line;

	//unclassify the rest deck 
	g0.unclassify_deck(&cd);
	g0.pass_deck(g1, &cd, declassify,g0);
	g1.pass_deck(g2, &g1.get_received_set(),declassify);
	printf("\n\nPlain text deck again\n");
	cd.print_content(); print_line;
	/**/

	return 0;
}
