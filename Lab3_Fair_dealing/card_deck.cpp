#include "StdAfx.h"
#include "card_deck.h"



classifiable_card card_deck::get_random_card()
{
	assert(!empty());
	
	shuffle();
	classifiable_card card = back();
	pop_back();
	return card;
}

void card_set::classify(const classifying_key key)
{
	std::for_each(begin(),end(),[&](classifiable_card& card) mutable
	{
		card.classify(key);
	});
}
void card_set::declassify(const classifying_key key)
{
	std::for_each(begin(),end(),[&] (classifiable_card& card) mutable
	{
		card.declassify(key);
	});
}

card_deck::card_deck()
{
	std::for_each(rank_begin, rank_end, 
		[&](card_rank rank)
		{
			std::for_each(suit_begin, suit_end, 
				[&](card_suit suit)
			{
				push_back(classifiable_card(rank, suit));
			});
			
		});
}
void card_deck::shuffle()
{
	std::random_shuffle(begin(),end());
}
void card_deck::classify(const classifying_key key)
{
	//shuffle();
	card_set::classify(key);
}