#include "StdAfx.h"
#include "card_enums.h"


card_rank &operator++(card_rank &c) {
	assert(c != rank_end);
  c = static_cast<card_rank>(c + 1);
  return c;
}

card_rank operator++(card_rank &c, int) {
  assert(c != rank_end); 
  ++c;
  return static_cast<card_rank>(c - 1);
}

card_rank operator*(card_rank c) {
  assert(c != rank_end);
  return c;
}


card_suit &operator++(card_suit &c) {
	assert(c != suit_end);
  c = static_cast<card_suit>(c + 1);
  return c;
}

card_suit operator++(card_suit &c, int) {
  assert(c != suit_end); 
  ++c;
  return static_cast<card_suit>(c - 1);
}

card_suit operator*(card_suit c) {
  assert(c != suit_end);
  return c;
}