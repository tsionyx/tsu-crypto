#pragma once
#include <vector>
#include <algorithm>

#include "classifiable_card.h"

class card_set: public std::vector<classifiable_card>
{
public:
	virtual void classify(const classifying_key key);
	virtual void declassify(const classifying_key key);
	void print_content() const
	{
		std::for_each(begin(), end(), [](classifiable_card card) { printf("%s\t", card.get_symbol());});
		printf("\n");
	}
};

class card_deck: public card_set
{
public:
	card_deck();
	void classify(const classifying_key key);
	void shuffle();
	classifiable_card get_random_card();
};

