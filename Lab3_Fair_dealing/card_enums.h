#pragma once

#include <iterator>
#include <assert.h>
enum card_rank: unsigned __int8
{
	rank_begin = 1,
	ace = rank_begin,
	two = 2,
	three = 3,
	four = 4,
	five = 5,
	six = 6,
	seven = 7,
	eight = 8,
	nine = 9,
	ten = 10,
	jack = 11,
	queen = 12,
	king = 13,
	rank_end
};

template<>
struct std::iterator_traits<card_rank>  {
  typedef card_rank  value_type;
  typedef int    difference_type;
  typedef card_rank *pointer;
  typedef card_rank &reference;
  typedef forward_iterator_tag
    iterator_category;
};


card_rank &operator++(card_rank &c);
card_rank operator++(card_rank &c, int);
card_rank operator*(card_rank c);


enum card_suit: unsigned __int8
{
	suit_begin = 3,
	hearts = suit_begin,
	diamonds = 4,
	clubs = 5,
	spades = 6,
	suit_end
};

template<>
struct std::iterator_traits<card_suit>  {
  typedef card_suit  value_type;
  typedef int    difference_type;
  typedef card_suit *pointer;
  typedef card_suit &reference;
  typedef forward_iterator_tag
    iterator_category;
};

card_suit &operator++(card_suit &c);
card_suit operator++(card_suit &c, int);
card_suit operator*(card_suit c);

