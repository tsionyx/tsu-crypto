#include "StdAfx.h"
#include "card.h"



card::card()
{
	throw "Not allowed to create default instance of the class";
}

card::card(card_rank rank, card_suit suit)
		//:_rank(rank),_suit(suit)
{
	_rank = rank, _suit = suit, v_un.v_un_s.therest = 0;
	build_symbol();
}
void card::build_symbol()
{
	switch(_rank)
	{
	case ace:
		symbol[0] = 'A';
		break;
	case two:
	case three:
	case four:
	case five:
	case six:
	case seven:
	case eight:
	case nine:
		symbol[0] = '0'+_rank;
		break;
	case ten:
		symbol[0] = 'T';
		break;
	case jack:
		symbol[0] = 'J';
		break;
	case queen:
		symbol[0] = 'Q';
		break;
	case king:
		symbol[0] = 'K';
		break;
	default:
		symbol[0] = 0;
		break;
	}

	switch(_suit)
	{
	case hearts:
	case diamonds:
	case clubs:
	case spades:
		symbol[1] = _suit;
		break;
	default:
		symbol[1] = 0;
		break;
	}
	symbol[2] = 0;
}