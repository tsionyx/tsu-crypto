#include "StdAfx.h"
#include "classifiable_card.h"
#include "..\Lab1_Large_numbers\__uintX2.h"

classifiable_card::classifiable_card()
	:card()
{
}
classifiable_card::classifiable_card(card_rank rank, card_suit suit)
	:card(rank,suit)
{
}

//#define SHOW_LOGS
#ifdef SHOW_LOGS
#define TERMINAL **/
#else 
#define TERMINAL /
#endif

#define show_log /TERMINAL printf 
void classifiable_card::classify(const classifying_key key)
{	
	classifiable_mixin::classify(key);
	
	show_log("\n====================\n");
	show_log("Classify the card %s with key {%i, %i}\n", symbol, key.key, key.p);

	key_type classified_value = this->_value;
	__uintX2<key_type> res = __uintX2<key_type>::pow_m(this->_value, key.key, key.p);

	show_log("%i ^ %i mod %i = ", classified_value, key.key, key.p);
	this->_value = res._l;
	show_log("%i;\n", this->_value);
	show_log("====================\n");
	build_symbol();
}
void classifiable_card::declassify(const classifying_key key)
{
	classifiable_mixin::declassify(key);

	show_log("\n====================\n");
	show_log("Declassify the card %s with key {%i, %i}\n", symbol, key.key, key.p);

	key_type classified_value = this->_value;
	__uintX2<key_type> res = __uintX2<key_type>::pow_m(this->_value, key.key, key.p);

	show_log("%i ^ %i mod %i = ", classified_value, key.key, key.p);
	this->_value = res._l;
	show_log("%i;\n", this->_value);
	show_log("====================\n");
	
	build_symbol();
}