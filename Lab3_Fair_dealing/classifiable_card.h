#pragma once
#include "card.h"
#include "classifiable_mixin.h"

class classifiable_card: public card, public classifiable_mixin
{
public:
	classifiable_card();
	classifiable_card(card_rank rank, card_suit suit);

	void classify(const classifying_key key);
	void declassify(const classifying_key key);

	void print_value()
	{
		printf("raw value is %i, formatted value is %s\n", _value, symbol);
	}
};
