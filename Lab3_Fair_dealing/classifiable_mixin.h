#pragma once

typedef unsigned int key_type;
typedef struct
{
	key_type key;
	key_type p;
} classifying_key;
class classifiable_mixin
{
private:
	unsigned int _classified;
public:
	virtual void classify(const classifying_key key)
	{
		++_classified;
	}
	virtual void declassify(const classifying_key key)
	{
		if(is_classified())
			--_classified;
		else throw "Item is not classified";
	}
	unsigned int get_classified_cardinality() const
	{
		return _classified;
	}
	bool is_classified() const
	{
		return (_classified != 0);
	}

};

