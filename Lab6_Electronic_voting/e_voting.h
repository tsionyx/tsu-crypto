#pragma once
#include "voter.h"
#include "tallier.h"

#include <vector>
using std::vector;

class e_voting
{
public:
	e_voting(vector<string> questions, size_t participants, size_t sets);
	void start();
	~e_voting();
protected:
	typedef vector<voter* > voters;

	tallier* _tallier;
	voters _voters;

	//size_t _q;
};

