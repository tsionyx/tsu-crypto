#pragma once
#include "tallier.h"

class voter
{
public:
	voter(tallier* const t, size_t questions, size_t sets);
	const messages& get_blinded_messages() const;
	const keys get_unblinded_keys(const size_t signed_index) const;

	const messages_set get_signed_messages_set(const messages_set& signed_blinded_messages_set);
	const signed_message make_choice();
	~voter();
protected:
	
	tallier* _t;
	size_t k, q;
	unsigned int s;
	size_t choozen_set;

	keys _ckey, _fkey;
	void generate_keys();

	messages _messages, blinded_messages;
	void generate_messages();
	void blind_messages();

	messages_set signed_messages;
	
};

