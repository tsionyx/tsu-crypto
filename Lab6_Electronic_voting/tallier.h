#pragma once
#include "../Lab5_EDS_RSA/RSA_System.h"

#include <vector>
#include <string>
using std::vector;
using std::string;

typedef uint128 key;
typedef vector<key> keys;

// Shaum scheme is y = E(x) = x^k mod n
// And it quite useless to encrypt X when its value is 0 or 1)
// Salt is used for hiding the obvious ciphering values
// Really, y = E(x+salt) and then x' = D(y) - salt

const unsigned int SALT = 20;

typedef uint128 message;
typedef vector<message> messages_set;
typedef vector< messages_set > messages;

typedef vector<string> questionnarie;
struct signed_message
{
	messages_set::value_type message, signature;
};


class tallier: public RSA_System
{
private:
	vector<unsigned int> _elections;
	questionnarie _questions;
public:
	tallier(questionnarie questions);
	const messages get_plain_messages(const messages& blinded_messages, const keys& unblinded_keys) const;
	const messages_set get_blind_signed_messages_set(const messages_set& blinded_messages_set) const;
	bool check_vote(signed_message);
	
	inline questionnarie get_questionnaire() const
	{ return _questions; }

	inline vector<unsigned int> get_results() const
	{ return _elections; }

};

