

#include "stdafx.h"
#include "e_voting.h"


int main(int argc, char* argv[])
{
	const size_t QUESTIONS = 4, VOTERS = 3, CHECKED_SETS = 3;

	questionnarie questions(QUESTIONS);
	for(size_t i = 0; i < questions.size(); ++i)
	{
		char num[3];
		_itoa_s(i, num, 10);
		questions[i] = string("Vote #").append(num);
	}
	

	e_voting e(questions, VOTERS, CHECKED_SETS);
	e.start();

	return 0;
}

