﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileTransfer
{
    static class SecureFileTransferProtocol
    {
        public static readonly string clFileSend = "SENDFILENAME";
        public static readonly string clFileSizeSend = "SENDFILESIZE";

        public static readonly string srvKeyLength = "KEYLENGTH";
        //public static readonly string srvKey = "KEY";
        
        public static readonly string clEncryptedFileLength = "ENCRYPTEDFILESIZE";
        //public static readonly string clEncryptedFile = "ENCRYPTEDFILE";

        public static readonly string clSymKeyLength = "SYMMETRICKEYLENGTH";
        //public static readonly string clSymKey = "SYMMETRICKEY";

        public static readonly string delimeter = "\r\n";

        public static readonly int readBufferLength = 1024;

    }
}
