﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace FileTransfer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }

   
    /********************       Console application         ********************/
     
    /*
         ****************** Protocol description *******************
         * C is a Client, S is a server
         * 
         * C -> S: "SENDFILENAME={file_name}\r\n"
         * C -> S: "SENDFILESIZE={file_size}\r\n"
         * S -> C: "KEYLENGTH{key_length}\r\n{key_content}"
         * 
         * C: ciphering
         *  
         * C -> S: "SYMMETRICKEYLENGTH={key_length}\r\n{key_content}"
         * C -> S: "ENCRYPTEDFILESIZE={file_size}\r\n{file_content}"
         * 
         * 
         * S: decrypting
         * S: save the file



        static void Main(string[] args)
        {
            if (args.Length < 1) return;

            if (args[0] == "client")
            {
                if (args.Length < 2) return;
                FileHoster fh = new FileHoster();

                string ip = "127.0.0.1";
                if (args.Length > 2)
                    ip = args[2];

                fh.SendFile(args[1], new IPEndPoint(IPAddress.Parse(ip), 3456));
            }
            else if (args[0] == "server")
            {
                FileReceiver fr = new FileReceiver(1024, 3456);
            }
        }
     */
}
