﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Net;
using FileTransfer.BlockCipher;


namespace FileTransfer
{
  
    public partial class MainForm : Form
    {

        private FileHoster fileHoster = null;
        private FileReceiver fileReceiver = null;

        public MainForm()
        {
            InitializeComponent();
        }

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFilePath.Text = openFileDialog1.FileName;

        }

        private void btnSendFile_Click(object sender, EventArgs e)
        {
            Algorithm algorithm = Algorithm.None;
            if(radBtnGost.Checked)
                algorithm = Algorithm.GOST;
            else if (radBtnBlowfish.Checked)
                algorithm = Algorithm.Blowfish;

            bool res = fileHoster.SendFile(txtFilePath.Text, new IPEndPoint(IPAddress.Parse(txtServerIp.Text), (int)numServerPort.Value), algorithm);


            if (res) MessageBox.Show("File was sended");
            else MessageBox.Show("Cannot send the file");
        }

        private void btnListen_Click(object sender, EventArgs e)
        {
            StartStop();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            fileHoster = new FileHoster();

            //StartStop();
        }

        private bool isRunning = false;
        private void StartStop()
        {
            if (isRunning)
            {
                fileReceiver.Stop();
                btnListen.Text = "Start";
                numListenPort.Enabled = true;
                isRunning = false;

            }
            else
            {
                StartOnPort((int)numListenPort.Value);
                btnListen.Text = "Stop";
                numListenPort.Enabled = false;
                isRunning = true;
            }
        }
        private void StartOnPort(int port)
        {
            if (fileReceiver == null)
            {
                fileReceiver = new FileReceiver(port);
                fileReceiver.LogGenerated += new LogEventHandler((sender, e) =>
                {

                    if (txtLog != null)
                    {
                        MethodInvoker invoker = new MethodInvoker(delegate()
                                                         {
                                                             txtLog.AppendText(e.MessageLog + Environment.NewLine);
                                                             txtLog.Update();
                                                             txtLog.ScrollToCaret();
                                                         });

                        txtLog.Invoke(invoker);
                    }
                });
            }
            else
            {
                fileReceiver.Stop();
                fileReceiver.Port = port;
            }

            fileReceiver.Start();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (fileReceiver != null)
                fileReceiver.Stop();
        }
    }
}
