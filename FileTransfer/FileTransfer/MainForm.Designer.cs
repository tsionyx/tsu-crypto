﻿namespace FileTransfer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnListen = new System.Windows.Forms.Button();
            this.numListenPort = new System.Windows.Forms.NumericUpDown();
            this.txtFilePath = new System.Windows.Forms.TextBox();
            this.numServerPort = new System.Windows.Forms.NumericUpDown();
            this.txtServerIp = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnSendFile = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.chooseAlgorithmGrpBox = new System.Windows.Forms.GroupBox();
            this.radBtnGost = new System.Windows.Forms.RadioButton();
            this.radBtnBlowfish = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.numListenPort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numServerPort)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.chooseAlgorithmGrpBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenFile.Location = new System.Drawing.Point(393, 33);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(75, 23);
            this.btnOpenFile.TabIndex = 0;
            this.btnOpenFile.Text = "Open";
            this.btnOpenFile.UseVisualStyleBackColor = true;
            this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnListen
            // 
            this.btnListen.Location = new System.Drawing.Point(96, 22);
            this.btnListen.Name = "btnListen";
            this.btnListen.Size = new System.Drawing.Size(75, 23);
            this.btnListen.TabIndex = 1;
            this.btnListen.Text = "Start";
            this.btnListen.UseVisualStyleBackColor = true;
            this.btnListen.Click += new System.EventHandler(this.btnListen_Click);
            // 
            // numListenPort
            // 
            this.numListenPort.Location = new System.Drawing.Point(15, 22);
            this.numListenPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.numListenPort.Name = "numListenPort";
            this.numListenPort.Size = new System.Drawing.Size(64, 20);
            this.numListenPort.TabIndex = 3;
            this.numListenPort.Value = new decimal(new int[] {
            3456,
            0,
            0,
            0});
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(25, 36);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.Size = new System.Drawing.Size(358, 20);
            this.txtFilePath.TabIndex = 4;
            // 
            // numServerPort
            // 
            this.numServerPort.Location = new System.Drawing.Point(199, 79);
            this.numServerPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.numServerPort.Name = "numServerPort";
            this.numServerPort.Size = new System.Drawing.Size(64, 20);
            this.numServerPort.TabIndex = 6;
            this.numServerPort.Value = new decimal(new int[] {
            3456,
            0,
            0,
            0});
            // 
            // txtServerIp
            // 
            this.txtServerIp.Location = new System.Drawing.Point(25, 79);
            this.txtServerIp.Name = "txtServerIp";
            this.txtServerIp.Size = new System.Drawing.Size(152, 20);
            this.txtServerIp.TabIndex = 5;
            this.txtServerIp.Text = "127.0.0.1";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(484, 302);
            this.tabControl1.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.chooseAlgorithmGrpBox);
            this.tabPage1.Controls.Add(this.btnSendFile);
            this.tabPage1.Controls.Add(this.numServerPort);
            this.tabPage1.Controls.Add(this.btnOpenFile);
            this.tabPage1.Controls.Add(this.txtServerIp);
            this.tabPage1.Controls.Add(this.txtFilePath);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(476, 276);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Client";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnSendFile
            // 
            this.btnSendFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendFile.Location = new System.Drawing.Point(393, 162);
            this.btnSendFile.Name = "btnSendFile";
            this.btnSendFile.Size = new System.Drawing.Size(75, 23);
            this.btnSendFile.TabIndex = 7;
            this.btnSendFile.Text = "Send file";
            this.btnSendFile.UseVisualStyleBackColor = true;
            this.btnSendFile.Click += new System.EventHandler(this.btnSendFile_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtLog);
            this.tabPage2.Controls.Add(this.numListenPort);
            this.tabPage2.Controls.Add(this.btnListen);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(476, 276);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Server";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtLog
            // 
            this.txtLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLog.Location = new System.Drawing.Point(3, 50);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLog.Size = new System.Drawing.Size(470, 223);
            this.txtLog.TabIndex = 4;
            // 
            // chooseAlgorithmGrpBox
            // 
            this.chooseAlgorithmGrpBox.Controls.Add(this.radBtnBlowfish);
            this.chooseAlgorithmGrpBox.Controls.Add(this.radBtnGost);
            this.chooseAlgorithmGrpBox.Location = new System.Drawing.Point(25, 118);
            this.chooseAlgorithmGrpBox.Name = "chooseAlgorithmGrpBox";
            this.chooseAlgorithmGrpBox.Size = new System.Drawing.Size(200, 100);
            this.chooseAlgorithmGrpBox.TabIndex = 8;
            this.chooseAlgorithmGrpBox.TabStop = false;
            this.chooseAlgorithmGrpBox.Text = "Algorithm";
            // 
            // radBtnGost
            // 
            this.radBtnGost.AutoSize = true;
            this.radBtnGost.Checked = true;
            this.radBtnGost.Location = new System.Drawing.Point(23, 20);
            this.radBtnGost.Name = "radBtnGost";
            this.radBtnGost.Size = new System.Drawing.Size(68, 17);
            this.radBtnGost.TabIndex = 0;
            this.radBtnGost.TabStop = true;
            this.radBtnGost.Text = "ГОСТ-89";
            this.radBtnGost.UseVisualStyleBackColor = true;
            // 
            // radBtnBlowfish
            // 
            this.radBtnBlowfish.AutoSize = true;
            this.radBtnBlowfish.Location = new System.Drawing.Point(23, 44);
            this.radBtnBlowfish.Name = "radBtnBlowfish";
            this.radBtnBlowfish.Size = new System.Drawing.Size(64, 17);
            this.radBtnBlowfish.TabIndex = 1;
            this.radBtnBlowfish.Text = "Blowfish";
            this.radBtnBlowfish.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AcceptButton = this.btnSendFile;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 302);
            this.Controls.Add(this.tabControl1);
            this.MinimumSize = new System.Drawing.Size(500, 340);
            this.Name = "MainForm";
            this.Text = "File Transfer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numListenPort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numServerPort)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.chooseAlgorithmGrpBox.ResumeLayout(false);
            this.chooseAlgorithmGrpBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnListen;
        private System.Windows.Forms.NumericUpDown numListenPort;
        private System.Windows.Forms.TextBox txtFilePath;
        private System.Windows.Forms.NumericUpDown numServerPort;
        private System.Windows.Forms.TextBox txtServerIp;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnSendFile;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.GroupBox chooseAlgorithmGrpBox;
        private System.Windows.Forms.RadioButton radBtnBlowfish;
        private System.Windows.Forms.RadioButton radBtnGost;
    }
}

